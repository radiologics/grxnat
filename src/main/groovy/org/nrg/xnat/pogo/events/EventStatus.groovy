package org.nrg.xnat.pogo.events

enum EventStatus {

    CREATED,
    DELETED,
    UPDATED

}
