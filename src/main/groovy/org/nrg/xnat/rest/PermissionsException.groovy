package org.nrg.xnat.rest

class PermissionsException extends RuntimeException {

    PermissionsException() {
        super()
    }

    PermissionsException(String message) {
        super(message)
    }

}
